class ReviewsController < ApplicationController
  load_and_authorize_resource :restaurant
  load_and_authorize_resource

  def create
    if @review.save
       flash[:success]= 'Created Successfuly'
      redirect_to restaurants_path
    else
      flash.now[:error]= @restaurant.errors.full_messages.to_sentence
      render :new
  end
  end

  def update
    if @review.update_attributes(review_params)
    flash[:success] = 'Reviewed Successfully'
    redirect_to restaurant_path
  else
    flash.now[:error]= @restaurant.errors.full_messages.to_sentence
    render :edit
  end
  end

  def destroy
    if @review.destroy
      flash[:success] = 'Deleted Successfuly'
      redirect_to review_path
    else
      flash.now[:error]= @review.error.full_messages.to_sentence
      render:delete
    end
  end


  private
  def review_params
    params.require(:review).permit(:description, :star).merge(user_id: current_user.id, restaurant_id: @restaurant.id)
  end
end
