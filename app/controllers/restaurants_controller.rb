class RestaurantsController < ApplicationController
 load_and_authorize_resource

def review
  if @restaurant.update_attributes(restaurant_params)
    flash[:success] = 'Reviewed Successfully'
    redirect_to restaurant_path
  else
    flash.now[:error]= @restaurant.errors.full_messages.to_sentence
    render :edit
  end
  end

  def create
    if @restaurant.save
      flash[:success]= 'Created Successfuly'
      redirect_to restaurants_path
    else
      flash.now[:error]= @restaurant.errors.full_messages.to_sentence
      render :new
    end

def comments
  @restaurant= Restaurant.find(params[:id])
  if @restaurant.update_attributes(params[:name])
    render 'reviews/new.html.haml'
  else
render :edit
  end
end
  end

    def restaurant_params
      params.require(:restaurant).permit(:name)
    end
def edit
end


def show
end

 def destroy
    if @restaurant.destroy
      flash[:success] = 'Deleted Successfuly'
      redirect_to restaurant_path
    else
      flash.now[:error]= @restaurant.error.full_messages.to_sentence
      render:delete
    end
  end


def new
  @restaurant= Restaurant.find(params[:id])
  if @restaurant.update_attributes(params[:name])
    render 'restaurants/index.html.haml'
  else
    render :edit
  end
end

  end

