class CategoriesController < ApplicationController
  load_and_authorize_resource

  def update
    if @category.update_attributes(category_params)
      flash[:success] = 'Updated Successfully'
      redirect_to categories_path
    else
      flash.now[:error] = @category.errors.full_messages.to_sentence
      render :edit
    end
  end

  def create
    if @category.save
      flash[:success] = 'Created Successfully'
      redirect_to categories_path
    else
      flash.now[:error] = @category.errors.full_messages.to_sentence
      render :new
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end
