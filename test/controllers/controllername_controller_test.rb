require 'test_helper'

class ControllernameControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get controllername_new_url
    assert_response :success
  end

  test "should get reviews" do
    get controllername_reviews_url
    assert_response :success
  end

end
